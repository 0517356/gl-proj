import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MyTabs from './components/NavigationBar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StatusBar } from 'react-native';
import Brand from './scr/Brand';
import Contact from './scr/Contact'
import About from './scr/About'
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
  return (
    <SafeAreaProvider>
      
      <StatusBar 
        backgroundColor='#A5D6AA'
        barStyle='default'/>
      
      <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="Home">
          <Stack.Screen name="Home" component={MyTabs} />
          <Stack.Screen name="Brand" component={Brand} />
          <Stack.Screen name="Contact" component={Contact} />
          <Stack.Screen name="About" component={About} />
        </Stack.Navigator>
      </NavigationContainer>
    
    </SafeAreaProvider>
  );
}
