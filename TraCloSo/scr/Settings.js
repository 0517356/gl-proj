import * as React from 'react';
import { useState } from "react";
import { StyleSheet, Text, Switch, View, TouchableOpacity, LayoutAnimation, UIManager } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import HeaderComponent from '../components/Header';
import Constants from 'expo-constants';
import { StatusBar } from 'expo-status-bar';
import Icon from "react-native-vector-icons/MaterialIcons";


export default function Settings() {
  const [isEnabledDarkMode, setIsEnabledDarkMode] = useState(false);
  const [isEnabledNotification, setIsEnabledNotification] = useState(true);
  const toggleSwitchDarkMode = () => setIsEnabledDarkMode(previousState => !previousState);
  const toggleSwitchNotification = () => setIsEnabledNotification(previousState => !previousState);
  const [expanded, setExpanded] = useState(false);

  if (
    Platform.OS === "android" &&
    UIManager.setLayoutAnimationEnabledExperimental
  ) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  return (
    <SafeAreaView style={style.container}>
      {/*header*/}
      <View style={style.Sheader}>
        <Text style={style.SheaderText}>Settings</Text>
      </View>
      <View style={style.SContent}>
        {/*dark mode*/}
        <View style={style.row}> 
          <Text style={style.Stext} >Dark mode</Text>
          <Switch
            style={style.switch}
            trackColor={{ false: "#767577", true: "#A5D6AA" }}
            thumbColor={isEnabledDarkMode ? "#FFFFFF" : "#FFFFFF"}
            ios_backgroundColor="#E5E5E5"
            onValueChange={toggleSwitchDarkMode}
            value={isEnabledDarkMode}
          />
        </View>
        {/*notification*/}
        <View style={style.row}>
          <Text style={style.Stext}>Notifications</Text>
          <Switch
            style={style.switch}
            trackColor={{ false: "#767577", true: "#A5D6AA" }}
            thumbColor={isEnabledNotification ? "#FFFFFF" : "#FFFFFF"}
            ios_backgroundColor="#E5E5E5"
            onValueChange={toggleSwitchNotification}
            value={isEnabledNotification}
          />
        </View>
        {/*language*/}
        <View style={style.collapse}>
          <TouchableOpacity 
            style={style.Accordion}
            onPress={() => {LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
              setExpanded(!expanded)}}
          >
            <View style={style.language}>
              <Text style={{ fontSize: 18 }}>Language</Text>
              <Icon name={expanded ? 'keyboard-arrow-up': 'keyboard-arrow-down'} style={style.ExIcon} size={30} color='#767577' />
            </View> 
          </TouchableOpacity> 
          {(expanded &&
            <View style={style.options}>
              <TouchableOpacity>
                <Text style={style.taal}>English</Text>
                <Icon name='check' style={style.CheckIcon} size={22} color='#767577'/>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={style.taal}>Nederlands</Text>
              </TouchableOpacity>
            </View>
          )}
          </View>
      </View>
    </SafeAreaView>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'flex-start', 
    backgroundColor: '#ffffff',
  },
  Sheader: {
    backgroundColor: '#A5D6AA',
    height: 65,
    flexDirection: 'row',
    justifyContent: 'space-between', //to display it 'rowwise'
  },
  SheaderText: {
    fontSize: 24,
    paddingLeft: '2%',
    ...Platform.select({
      ios: {            
        marginTop: 25,
      },
      android: {
        marginTop: 20,
      }
    })
  },
  SContent:{
    marginTop: 20,
    marginLeft: 20,
    marginRight: 15
  },
  language: {
    textTransform: 'uppercase',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 15,
  },
  taal:{
    fontSize: 15,
    padding: 7,
    borderBottomWidth: 0.8,
    borderBottomColor: '#C6C6C9'
  },
  options: {
    padding: 5,
    paddingLeft: 20,
    paddingRight: 15

  },
  row: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60,
  },
  switch: {
    ...Platform.select({
      android: {
        transform: [{scaleX: 1.2}, {scaleY:1.2}],
      },
      ios: {
        transform: [{scaleX: 0.8}, {scaleY:0.8}],
        marginTop: 5
      }
    }),    
    marginRight: 10
  },
  Stext: {
    fontSize: 18,
    marginTop: 10
  },
  collapse: {
    marginTop: 10
  },
  CheckIcon: {
    position: 'absolute',
    right: 4,
    ...Platform.select({
      ios: {            
        marginTop: 6,
      },
      android: {
        marginTop: 12,
      }
    })
  },
  ExIcon: {
    ...Platform.select({
      ios: {            
        marginTop: -1,
      },
      android: {
        marginTop: 2,
      }
    })
  }
})
