import * as React from 'react';
import {useEffect, useState} from 'react';
import { SafeAreaView, Platform, StyleSheet, Share, AsyncStorage,
					TouchableOpacity, Text, View, Image, Dimensions, ScrollView } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { Overlay } from 'react-native-elements';
import { showMessage, hideMessage } from "react-native-flash-message";
import FlashMessage from "react-native-flash-message";
import firebase from 'firebase/app'; 


export default function BrandScreen({ navigation, route }) {
	const {BrandName, Score, Logo, Summary, Updated, Markers} = route.params
	const [favorite, setFavorite] = React.useState(false);

	const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          'TraCloSo | Building towards a better future for clothing. \n\nTake a look at: https://tracloso.com',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }}

  let transparencyColor;
  if (Score <= 100 && Score >= 70) {
    var transparancyColor = '#52D858'
	} else if(Score < 70 &&  Score >= 50) {
    var transparancyColor = '#F2B05C'
	} else {
    var transparancyColor = '#FF918E'
	} 

  const [visible, setVisible] = React.useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  var unformattedDate = Updated.seconds;
  var t = new Date(unformattedDate * 1000);
  var dateFormatted = ('0' + t.getDate()).slice(-2) + '-' + ('0' + t.getMonth()).slice(-2) + '-' + ('0' + t.getYear()).slice(-2) + ' ' + ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2);


  const addToFavorites = () => {
  	setFavorite(!favorite)
  	// message for adding/deleting a brand to/from favorites
  	favorite ? showMessage({
	    message: "Brand deleted from favorites",
	    icon: 'danger',
	    backgroundColor: '#A5D6AA',
	    color: 'black',
	    duration: 1000
	  })
	  :
	  showMessage({
	    message: "Brand added to favorites",
	    icon: 'success',
	    backgroundColor: '#A5D6AA',
	    color: 'black',
	    duration: 900
	  });

	  let FavoritesInformation = {
	  	BrandName: BrandName,
	  	Logo: Logo,
	  	Score: Score,
	  	Summary: Summary,
	  	Updated: Updated,
	  }

	  AsyncStorage.setItem(
	  	'brands',
	  	JSON.stringify(FavoritesInformation))
  }

  const [favoriteBrands, setFavoriteBrands] = React.useState({})

  // see if brand is in favorite, if so setFavorite to true
  useEffect(() => {
	  AsyncStorage.getItem('brands', (err, result) => {
	  	if (result !== null) {
		  	result = JSON.parse(result)
		  	setFavoriteBrands(result)
	  		if (favoriteBrands.BrandName !== null ) {
		  		if (BrandName == favoriteBrands.BrandName) {
		  			setFavorite(true)
	  			}
	  		}
	  	}
  	})
  })
	
	if (BrandName == "Adidas") {
		var getMarker = require('../assets/dataAdidas.json')
	} else if (BrandName == "H&M") {
		var getMarker = require('../assets/dataHM.json')
	} else if (BrandName == "Nike") {
		var getMarker = require('../assets/dataNike.json')
	} else if (BrandName == "C&A") {
		var getMarker = require('../assets/dataCA.json')
	}

	const markers = getMarker

	return(
		<SafeAreaView style={style.Container}>
			{/*header */}
			<View style={style.BrandHeader}>
				<View style={{flexDirection: 'row'}}>
					<Image 
						source={{uri: Logo}}
						style={style.BrandLogo}	
						/>
					<Text style={style.BrandText}>{BrandName}</Text>
				</View>
				<View style={style.Icons}>
					{/*favorite button*/}
					<TouchableOpacity onPress={addToFavorites}>
						<MaterialCommunityIcons style={{paddingRight: '5%'}} name={favorite ? 'star': 'star-outline'} size={30}/>
					</TouchableOpacity>
					{/*share button*/}
					<TouchableOpacity onPress={onShare}>
						<MaterialCommunityIcons name="share-variant" size={30}/>
					</TouchableOpacity>
				</View>
				<FlashMessage position="top" /> 
			</View>
			
			{/*block*/}
			<ScrollView>
				<View style={{flexDirection: 'row', justifyContent:'space-between'}}>
						{/*back button*/}
					<View>
						<TouchableOpacity style={style.BackButton} onPress={() => navigation.goBack()}>
							<MaterialCommunityIcons style={style.Arrow} name="chevron-left" size={18}/>
							<Text>back</Text>
						</TouchableOpacity>
						<Text style={style.UpdateText}>Last updated on {dateFormatted}</Text>
					</View>
						{/*transparancy score*/}
					<View style={[style.transparancyBox, {shadowColor: transparancyColor}]}>
						<Text style={[style.transparancyText, {color: transparancyColor}]}>{Score}%</Text>
					</View>
				</View>
				{/*content*/}
				<View style={style.content}>
					{/*summary*/}
					<Text style={style.header}>Summary</Text>
					<Text>{Summary}
								{"\n"} {"\n"}
								More info about how the score is calculated? Take a look at our information page</Text>
					{/*facilities*/}
					<Text style={[style.header, {paddingTop:15}]}>Facilities</Text>
					<Text>Click on one of the markers to see how much factories {BrandName} has in that country</Text>
					<View style={style.mapview}>
						<MapView style={style.Map}
					    initialRegion={{
						    latitude: 35.3260685,
						    longitude: 11.77832031,
						    latitudeDelta: 90,
						    longitudeDelta: 90,
						  }}>
							{markers.map((item, index) => (
								<Marker
									key={index}
							 		coordinate={item.coordinates}
							 		title={item.title}
							 	/>
								))}
						</MapView>
						<TouchableOpacity style={style.ExpandIcon} onPress={toggleOverlay}>
							<MaterialCommunityIcons  name='arrow-expand-all' size={25}/>
						</TouchableOpacity>
					</View>
					{/*map overlay: when clicking the expand icon*/}
					<View>	
						<Overlay 
							fullScreen= 'true' 
							isVisible={visible} 
							onBackdropPress={toggleOverlay}
							backdropStyle={style.BackDrop}
							overlayStyle={style.OverlayStyle} >
							<View>
								<MapView style={style.Zoom}
								initialRegion={{
						    latitude: 35.3260685,
						    longitude: 11.77832031,
						    latitudeDelta: 90,
						    longitudeDelta: 90,
						  }}>
									{markers.map((item, index) => (
										<Marker
											key={index}
									 		coordinate={item.coordinates}
									 		title={item.title}
									 	/>
										))}
								</MapView>
								<TouchableOpacity style={style.CloseIcon} onPress={toggleOverlay}>
									<MaterialCommunityIcons  name='close' size={25} color={'red'}/>
									<Text style={style.CloseText}>Close</Text>
								</TouchableOpacity>
							</View>
						</Overlay>
					</View>					
				</View>
			</ScrollView>
		</SafeAreaView>
	)
}

const style = StyleSheet.create ({
	Container: {
		flex: 1,
		backgroundColor: '#ffffff'
	},
	BrandHeader: {
  	backgroundColor: '#A5D6AA',
  	height: 65,
  	flexDirection: 'row',
  	justifyContent: 'space-between',
	},
	BrandLogo: {
		marginLeft: 5,
		resizeMode: 'contain',
		width: 70,
		height: 70
	},
	BrandText: {
		fontSize: 24,
		paddingLeft: '2%',
		...Platform.select({
			ios: {						
				marginTop: 25,
			},
			android: {
				marginTop: 20,
			}
		})
	},
	Icons: {
		flexDirection: 'row',
		marginTop: 25,
		marginRight: '4%'
	},
	BackButton: {
		flexDirection: 'row',
		marginLeft: 10,
		marginTop: 10
	},
	Arrow: {
		...Platform.select({
			android: {
				marginTop: 2,
			}
		})
	},
	transparancyBox: {
		backgroundColor: '#ffffff',
		height: 30,
		borderRadius: 100,
		width: 70,
		shadowOffset: {
			width: 0,
			height: 12,
		},
		shadowOpacity: 0.58	,
		shadowRadius: 16,
		elevation: 24,
		marginRight: 20,
		marginTop: 20
	},
	transparancyText: {
		textAlign: 'center',
		justifyContent: 'center',
		fontSize: 16,
		...Platform.select({
			ios: {						
				marginTop: '7%',
			},
			android: {
				marginTop: '3%',
			}
		})
	},
	UpdateText: {
		marginLeft: '5%',
		marginTop: 3,
		color: '#979797',
		fontStyle: 'italic',
		fontSize: 12
	},
	Map: {
		width: Dimensions.get('window').width-20,
		height: 200,
		alignItems: 'center',
		justifyContent: 'center',
	},
	mapview: {
		paddingTop: 5
	},
	content: {
		marginTop: 20,
		marginLeft: 10,
		marginRight: 2
	},
	header: {
		fontSize: 22
	},
	ExpandIcon: {
		position: 'absolute',
		right: 15,
		marginTop: 10
	},
	Zoom :{
		marginTop: 70,
		width: Dimensions.get('window').width-20,
		height: Dimensions.get('window').height-100
	},
	OverlayStyle: {
		backgroundColor: 'transparent'
	},
	CloseIcon: {
		position: 'absolute',
		right: 20,
		marginTop: 80,
		flexDirection: 'row'
	},
	CloseText: {
		...Platform.select({
			ios :{
				paddingTop: 4
			},
			android: {
				paddingTop: 2
			}
		}),
		color: 'red'
	}
})