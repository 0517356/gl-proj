import * as React from 'react';
import {useState, useEffect} from 'react';
import { SafeAreaView, StyleSheet, Text, FlatList, Image, TouchableOpacity } from 'react-native';
import HeaderComponent from '../components/Header';
import Card from '../components/Card'; 
import firebase from 'firebase/app';

//view for the homescreen
export default function Home({ navigation }) {
  const [
    selectedIndex,
    setSelectedIndex
  ] = React.useState(0);

  const [filteredDataSourceAZ, setFilteredDataSourceAZ] = useState([]);
  const [masterDataSourceAZ, setMasterDataSourceAZ] = useState([]);

  const [filteredDataSourceHighest, setFilteredDataSourceHighest] = useState([]);
  const [masterDataSourceHighest, setMasterDataSourceHighest] = useState([]);

  const [filteredDataSourceSport, setFilteredDataSourceSport] = useState([]);
  const [masterDataSourceSport, setMasterDataSourceSport] = useState([]);

  // connection with database
  const firebaseConfig = {
      apiKey: "AIzaSyDUlK-IJcotasPrL9WUqCO1Z86JufUNwJE",
      authDomain: "tracloso.firebaseapp.com",
      databaseURL: "https://tracloso-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "tracloso",
      storageBucket: "tracloso.appspot.com",
      messagingSenderId: "174173890413",
      appId: "1:174173890413:web:0df0a89c1472cb3cff1c64",
      measurementId: "G-EVHYYE4775"
    };

  if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig)
  }

  // retieving A-Z
  useEffect(() => {
  //retrieve record from firebase database
    firebase.firestore().collection('Brand').orderBy('brand_name').get().then((querySnapshot) => {
      let temp = []
      
      querySnapshot.forEach(snapshot => {
        let brandDetails = {};
        brandDetails = snapshot.data();
        temp.push(brandDetails);
        setFilteredDataSourceAZ(temp);
        setMasterDataSourceAZ(temp);
      });
    });
  }, []);

  // retieving highest
  useEffect(() => {
  //retrieve record from firebase database
    firebase.firestore().collection('Brand').orderBy('brand_score', 'desc').get().then((querySnapshot) => {
      let temp = []
      
      querySnapshot.forEach(snapshot => {
        let brandDetails = {};
        brandDetails = snapshot.data();
        temp.push(brandDetails);
        setFilteredDataSourceHighest(temp);
        setMasterDataSourceHighest(temp);
      });
    });
  }, []);

  // retieving only sport
  useEffect(() => {
  //retrieve record from firebase database
    firebase.firestore().collection("Brand").where("brand_sport", "==", true).get().then((querySnapshot) => {
      let temp = []
      
      querySnapshot.forEach(snapshot => {
        let brandDetails = {};
        brandDetails = snapshot.data();
        temp.push(brandDetails);
        setFilteredDataSourceSport(temp);
        setMasterDataSourceSport(temp);
      });
    });
  }, []);

  if (selectedIndex == 0) {
    var listToRender = filteredDataSourceAZ
  } else if (selectedIndex == 1) {
    var listToRender = filteredDataSourceHighest
  } else {
    var listToRender = filteredDataSourceSport
  }

  // to display the cards with brands
  let renderCard = ({ item }) => {
    return(
      <Card
        BrandName = {item.brand_name}
        Score = {item.brand_score}
        Logo = {item.brand_logo}
        Summary = {item.brand_summary}
        Updated = {item.updated_at}
        Markers = {item.brand_facilities}
      />
    );
  };

  // search function 
  const [search, setSearch] = useState('');

  const searchFilterFunction = (text) => {
    // search function for A-Z
    if (selectedIndex == 0) {
      if (text) {
        // Inserted text is not blank
        // Filter the masterDataSource
        // Update FilteredDataSource
        const newData = masterDataSourceAZ.filter(function (item) {
          const itemData = item.brand_name
            ? item.brand_name.toUpperCase()
            : ''.toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });
        setFilteredDataSourceAZ(newData);
        setSearch(text);
      } else {
        // Inserted text is blank
        // Update FilteredDataSource with masterDataSource
        setFilteredDataSourceAZ(masterDataSourceAZ);
        setSearch(text);
        }
      }
      // search function for Highest
    else if (selectedIndex == 1) {
      if (text) {
        // Inserted text is not blank
        // Filter the masterDataSource
        // Update FilteredDataSource
        const newData = masterDataSourceHighest.filter(function (item) {
          const itemData = item.brand_name
            ? item.brand_name.toUpperCase()
            : ''.toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });
        setFilteredDataSourceHighest(newData);
        setSearch(text);
      } else {
        // Inserted text is blank
        // Update FilteredDataSource with masterDataSource
        setFilteredDataSourceHighest(masterDataSourceHighest);
        setSearch(text);
      }
      // serach function for only sporting brands 
    } else {
      if (text) {
        // Inserted text is not blank
        // Filter the masterDataSource
        // Update FilteredDataSource
        const newData = masterDataSourceSport.filter(function (item) {
          const itemData = item.brand_name
            ? item.brand_name.toUpperCase()
            : ''.toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });
        setFilteredDataSourceSport(newData);
        setSearch(text);
      } else {
        // Inserted text is blank
        // Update FilteredDataSource with masterDataSource
        setFilteredDataSourceSport(masterDataSourceSport);
        setSearch(text);
        }
    }
  };

  return (
  	//this 'SafeAreaView' is required to render it safe on the screen, within the margins
  	<SafeAreaView forceInset={{ bottom: 'always' }} style={style.container}>
	    <HeaderComponent 
	    	selectedIndex={selectedIndex}   
	    	setSelectedIndex={setSelectedIndex}
        onChangeText={(text) => searchFilterFunction(text)}
        value={search}
	    />
    	<FlatList
        data={listToRender}
        keyExtractor={(item, index) => index.toString()}
        renderItem = {renderCard}
      />
	  </SafeAreaView>
  );
}

//style props of the header
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
})