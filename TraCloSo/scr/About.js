import * as React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from "react-native-vector-icons/AntDesign";

//view for the About screen
export default function About({navigation}) {
  return (
    <SafeAreaView style={style.container}>
      <View style={style.AHeader}>
        <Text style={style.AHeaderText}>About</Text>
      </View>
      <TouchableOpacity style={style.BackButton} onPress={() => navigation.goBack()}>
        <MaterialCommunityIcons style={style.Arrow} name="chevron-left" size={18}/>
        <Text>back</Text>
      </TouchableOpacity>
      <View style={style.content}>  
        <Text style={style.title}>Who are we?</Text>
          <Text>
            We are a young team of students studying Information Science. We have developed this app for our introductory project. After the project we developed the app further into the current form. We chose such kind of app, because we all are self-conscious about sustainability and think we could force the fashion industry towards being more sustainable.
          </Text>
          <Text style={[style.title, {paddingTop:15}]}>What is our mission?</Text>
            <Text>
              “We want to change the fashion industry. The way we challenge the fashion industry is by providing transparency about the origin of the clothing. Knowing where your clothes are made, and no more bad working conditions. Want to use our app to help?” {"\n"} {"\n"}
              Our mission is to show and indicate the transparency about the origin of the clothes of clothing brands. This is done by researching the origin of the clothing. Close cooperation with the companies by doing interviews with brands and NGO’s uncovers unknown information. Our purpose is to create a better world, where customers and society are aware of where and how their clothes are made. Also, where companies and brands are encouraged and steered to improve the environment and the working conditions of the laborers and to be more transparent. And last but not least, where improved sustainability is of importance.   
            </Text>
        </View>
    </SafeAreaView>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#ffffff'
  },
  AHeader: {
    backgroundColor: '#A5D6AA',
    height: 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  AHeaderText: {
    fontSize: 24,
    paddingLeft: '2%',
    ...Platform.select({
      ios: {            
        marginTop: 25,
      },
      android: {
        marginTop: 20,
      }
    })
  },
  title: {
    fontSize: 18,
    marginBottom: 10,
   },
   content: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 15
  },
  BackButton: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10
  },
  Arrow: {
    ...Platform.select({
      android: {
        marginTop: 2,
      }
    })
  },
})