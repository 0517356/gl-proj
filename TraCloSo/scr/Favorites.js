import * as React from 'react';
import { StyleSheet, Text, View, AsyncStorage, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import HeaderComponent from '../components/Header';
import firebase from 'firebase/app';
import Card from '../components/Card'

export default function Favorites() {
  const [
    selectedIndex,
    setSelectedIndex
  ] = React.useState(0);

  const [filteredDataSourceAZ, setFilteredDataSourceAZ] = React.useState([]);
  const [masterDataSourceAZ, setMasterDataSourceAZ] = React.useState([]);
  const [search, setSearch] = React.useState('');

  const searchFilterFunction = (text) => {
    // search function for A-Z
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource
      // Update FilteredDataSource
      const newData = masterDataSourceAZ.filter(function (item) {
        const itemData = item.brand_name
          ? item.brand_name.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSourceAZ(newData);
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredDataSourceAZ([]);
      setSearch(text);
      }
    }

  let renderCard = ({ item }) => {
    return(
      <Card
        BrandName = {item.brand_name}
        Score = {item.brand_score}
        Logo = {item.brand_logo}
        Summary = {item.brand_summary}
        Updated = {item.updated_at}
      />
    );
  };
  
   const firebaseConfig = {
      apiKey: "AIzaSyDUlK-IJcotasPrL9WUqCO1Z86JufUNwJE",
      authDomain: "tracloso.firebaseapp.com",
      databaseURL: "https://tracloso-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "tracloso",
      storageBucket: "tracloso.appspot.com",
      messagingSenderId: "174173890413",
      appId: "1:174173890413:web:0df0a89c1472cb3cff1c64",
      measurementId: "G-EVHYYE4775"
    };

  if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig)
  }

  // retieving A-Z
  React.useEffect(() => {
  //retrieve record from firebase database
    firebase.firestore().collection('Brand').orderBy('brand_name').get().then((querySnapshot) => {
      let temp = []
      
      querySnapshot.forEach(snapshot => {
        let brandDetails = {};
        brandDetails = snapshot.data();
        temp.push(brandDetails);
        setMasterDataSourceAZ(temp);
      });
    });
  }, []);

  
  const [favoriteItems, setFavoriteItems] = React.useState({})

  // get favorites
  AsyncStorage.getItem('brands', (err, result) => {
    if (result !== null) {
      result = JSON.parse(result)
      setFavoriteItems(result)
    }
  });
  
  return (
    <SafeAreaView style={style.container}>
	    
	    <HeaderComponent 
	    	selectedIndex={selectedIndex} 
	    	setSelectedIndex={setSelectedIndex}
        selectedIndex={selectedIndex}   
        setSelectedIndex={setSelectedIndex}
        onChangeText={(text) => searchFilterFunction(text)}
        value={search}
	    />
      <Card
        BrandName={favoriteItems.BrandName}
        Logo={favoriteItems.Logo}
        Score={favoriteItems.Score}
        Summary={favoriteItems.Summary}
        Updated={favoriteItems.Updated}
      />
      <FlatList
        data={filteredDataSourceAZ}
        keyExtractor={(item, index) => index.toString()}
        renderItem = {renderCard}
      />
	  
	  </SafeAreaView>
  );
}

const style = StyleSheet.create({
  container: {
  	flex: 1,
    backgroundColor: '#ffffff'
  },
})