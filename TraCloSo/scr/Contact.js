import * as React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from "react-native-vector-icons/Entypo";

//view for the Contact screen
export default function Contact( {navigation}) {
  return (
    <SafeAreaView style={style.container}>
      <View style={style.CHeader}>
        <Text style={style.CHeaderText}>Contact</Text>
      </View>
      <TouchableOpacity style={style.BackButton} onPress={() => navigation.goBack()}>
        <MaterialCommunityIcons style={style.Arrow} name="chevron-left" size={18}/>
        <Text>back</Text>
      </TouchableOpacity>
      <View style={style.Content}>
        <View style={style.Row}>  
          <Icon name='phone' size={25} />
          <Text style={style.Space}>+31 030 253 3550</Text>
        </View>
        <View style={style.Row}>
          <Icon name='email' size={25} />
          <Text style={style.Space} >info@tracloso.com</Text>
        </View>
        <View style={style.Row}>
          <Icon name='twitter' size={25} />
          <Text style={style.Space} >@tracloso</Text>
        </View>
        <View style={style.Row}>
          <Icon name='facebook' size={25} />
          <Text style={style.Space} >Tracloso</Text>
        </View>
        <View style={style.Row}>
          <Icon name='instagram' size={25} />
          <Text style={style.Space} >@Tracloso</Text>
        </View>
        <View style={style.Row}>
          <Icon name='linkedin' size={25} />
          <Text style={style.Space} >Tracloso</Text>
        </View>
      </View>
    </SafeAreaView>  
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#ffffff'
  },
  CHeader: {
    backgroundColor: '#A5D6AA',
    height: 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  CHeaderText: {
    fontSize: 24,
    paddingLeft: '2%',
    ...Platform.select({
      ios: {            
        marginTop: 25,
      },
      android: {
        marginTop: 20,
      }
    })
  },
  BackButton: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10
  },
  Arrow: {
    ...Platform.select({
      android: {
        marginTop: 2,
      }
    })
  },
  Content: {
    marginTop: 10
  },
  Row: {
    flexDirection: 'row',
    margin: 10,
    marginLeft: 25,
  },
  Space: {
    marginLeft: 10,
    marginTop: 5,
    fontSize: 15,
  },
})