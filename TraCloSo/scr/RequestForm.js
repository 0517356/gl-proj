import * as React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Overlay } from 'react-native-elements';
import firebase from 'firebase/app';
import 'firebase/firestore'

//view for the requestform
export default function RequestForm() {

  const [Name, onChangeName] = React.useState("")
  const [Mail, onChangeMail] = React.useState("")
  const [Motivation, onChangeMotivation] = React.useState("")

  const [visible, setVisible] = React.useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const checkTextInput = () => {
    //Check for the Name TextInput
    if (Name == "") {
      alert('Please enter a company name');
      return;
    }
    //Check for the Email TextInput
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+(\s*)$/;
    if (reg.test(Mail) === false ) {
      alert('Please enter a correct e-mail adress');
      return;
    }
    if (Motivation == "") {
      alert('Please enter your motivation');
      return;
    }
    //Checked Successfully 
    const firebaseConfig = {
      apiKey: "AIzaSyDUlK-IJcotasPrL9WUqCO1Z86JufUNwJE",
      authDomain: "tracloso.firebaseapp.com",
      databaseURL: "https://tracloso-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "tracloso",
      storageBucket: "tracloso.appspot.com",
      messagingSenderId: "174173890413",
      appId: "1:174173890413:web:0df0a89c1472cb3cff1c64",
      measurementId: "G-EVHYYE4775"
    };

    if (firebase.apps.length === 0) {
      firebase.initializeApp(firebaseConfig)
    }
    //store record in firebase database
    firebase.firestore().collection('Form').add({
      name: Name,
      email: Mail,
      motivation: Motivation,
      created_at: firebase.firestore.FieldValue.serverTimestamp()
    });

    //send email using php file 
    fetch('https://webspace.science.uu.nl/~8571333/tracloso/form.php', {
      method: 'Post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: Name,
        email: Mail,
        motivation: Motivation
      })
    })

    Alert.alert("Your request has been received!",
      `We'll send you a conformation email and we keep you updated through the mail. You can also find more information on our social media!`)

    //empty fields
    onChangeName('');
    onChangeMail('');
    onChangeMotivation('');

    };

  var ButtonColor;
  if (Name == '') {
    ButtonColor = '#E5E5E5';
    var buttonDisabled = true
  } else if (Mail == '') {
    ButtonColor = '#E5E5E5';
    var buttonDisabled = true
  } else if (Motivation == '') {
    ButtonColor = '#E5E5E5';
    var buttonDisabled = true
  } else {
    ButtonColor = '#A5D6AA'
    var buttonDisabled = false
  }

  return (
    <SafeAreaView style={style.container}>
      {/*header*/}
      <View style={style.FormHeader}>
        <Text style={style.HeaderText}>Brand Request Form</Text>
        <TouchableOpacity style={style.Icons} onPress={toggleOverlay}>
          <MaterialCommunityIcons name="help-circle-outline" size={30}/>
        </TouchableOpacity>
      </View>
      {/*form*/}
      <View style={style.Form}>
        <View style={style.InputComponent}>
          <Text style={style.InputText}>Company Name *</Text>
          <TextInput
            style={style.input}
            onChangeText={onChangeName}
            value={Name}
            placeholder="ex. TraCloSo"
          />
        </View>
        <View style={style.InputComponent}>
          <View>
            <Text style={style.InputText}>Your email (on which we can contact you) *</Text>
          </View>
          <TextInput
            style={style.input}
            onChangeText={onChangeMail}
            value={Mail}
            placeholder="example@domain.com"
            autoCompleteType='email'
            textContentType='emailAddress'
          />
        </View>
        <View style={style.InputComponent}>
          <Text style={style.InputText}>Why do you want this brand to be researched? *</Text>
          <TextInput
            style={[style.input, {borderWidth: 1, textAlignVertical: 'top', paddingTop: 5, height: 150}]}
            onChangeText={onChangeMotivation}
            value={Motivation}
            placeholder="Your explanation..."
            multiline= {true}
          />
        </View>
        {/*send button*/}
        <View style={{alignItems: 'center',}}>
          <TouchableOpacity onPress={checkTextInput} style={[style.SubmitButton, {backgroundColor: ButtonColor}]} disabled={buttonDisabled}>
            <Text style={style.ButtonText}>Send</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/*overlay for help*/}
      <View>  
        <Overlay 
          fullScreen= 'true' 
          isVisible={visible} 
          onBackdropPress={toggleOverlay}
          backdropStyle={style.BackDrop}
          overlayStyle={style.OverlayStyle} >
          <View style={{alignItems: 'center'}}>
            <View style={style.help}>
              <View style={style.helpheader}>
                <Text style={style.helpHeaderText}>What is this form for?</Text>
              </View>
              <View style={style.headerContent}>
                <Text style={{fontSize:18, textAlign: 'center', fontWeight: '300'}}>With this form you can request for a brand to be researched. Simply fill in the brand (already existing in the app, or a new brand), your email and motivation. We need your email in case we want to contact you. {"\n"} {"\n"}More info? Send us an email!</Text>
                <Text></Text>
                <MaterialCommunityIcons  name='help-circle-outline' size={50} color={'black'}/>
              </View>
            </View>
            <TouchableOpacity style={style.CloseIcon} onPress={toggleOverlay}>
              <MaterialCommunityIcons  name='close' size={25} color={'black'}/>
            </TouchableOpacity>
          </View>
        </Overlay>
      </View>
    </SafeAreaView>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#ffffff'
  },
  FormHeader: {
    backgroundColor: '#A5D6AA',
    height: 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  HeaderText: {
    fontSize: 24,
    paddingLeft: '2%',
    ...Platform.select({
      ios: {            
        marginTop: 25,
      },
      android: {
        marginTop: 20,
      }
    })
  },
  Icons: {
    marginTop: 25,
    marginRight: '3%'
  },
  Form: {
    justifyContent: 'center',
  },
  input: {
    borderBottomWidth: 1,
    borderColor: '#C6C6C9',
    width: '90%',
    marginLeft: '5%',
    paddingLeft: 3,
    height: 35
  },
  InputComponent: {
    marginTop: 25,
  },
  InputText: {
    marginLeft: 10,
    fontSize: 14,
  },
  SubmitButton: {
    marginTop: 30,
    borderRadius: 13,
    height: 55,
    width: '80%',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ButtonText: {
    textAlign: 'center',
    fontSize: 18
  },
  help: {
    height: '70%',
    width: '85%',
    backgroundColor: '#ffffff',
    marginTop: 90
  }, 
  OverlayStyle: {
    backgroundColor: 'transparent',
  },
  CloseIcon: {
    position: 'absolute',
    right: '10%',
    marginTop: 95,
    flexDirection: 'row'
  },
  CloseText: {
    paddingTop: 2,
    color: 'black'
  },
  helpheader: {
    height: '25%',
    width: '100%',
    backgroundColor: '#D1EFD6',
    position: 'absolute',
    alignItems: 'center'
  },
  helpHeaderText: {
    marginTop: 40,
    fontSize: 22
  },
  headerContent: {
    marginTop: '40%',
    alignItems: 'center',
  }
})