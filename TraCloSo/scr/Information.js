import * as React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from "react-native-vector-icons/AntDesign";
import { Divider } from "react-native-elements";

//view for the information screen
export default function Information({navigation}) {
  return (
    <SafeAreaView style={style.container}>
      <View style={style.IHeader}>
        <Text style={style.IHeaderText}>Information</Text>
      </View>

      <ScrollView>
        <View style={style.content}>
            {/*question 1*/}
            <Text style={style.title}>How did we get the information?</Text>
            <Text>
                  The information we are giving, is gathered by doing research. This research exists out of interviews done with clothing brands, stores or agents that have information about the origin of clothes. Moreover, we are in close cooperation with companies like us. Together we could better influence the fashion industry towards a sustainable industry.
                  Furthermore, we have taken the claims of the brands self and searched for any information supporting those claims. {"\n"}The results of the research are presented in an insightful way in this app!
            </Text>
            {/*question 2*/}
            <Text style={[style.title, {paddingTop:15}]}>What does the score mean?</Text>
            <Text style={{marginBottom: 20}}>
                  This score is just an indicator of how clothing brands are doing related to multiple aspects such as employees and environment. We made use of a method called 'B impact Assessment'. The questions they use help a lot in assessing the transparency of brands, thus, we were able to use a couple of those.
                  We used the questions that are applicable to this certain case.  
            </Text>
            {/*link to about and contact page*/}
            <Divider style={{marginTop: 5}}/>
            <TouchableOpacity style={style.Click} onPress={() => navigation.navigate('About')}>
              <Text style={{paddingTop:5, fontSize:17}}>About</Text>   
              <Icon name='right' size={18} style={{paddingTop: '2%'}} color='#767577'/>       
            </TouchableOpacity>
            <Divider style={{marginTop: 8}}/>
            <TouchableOpacity style={style.Click} onPress={() => navigation.navigate('Contact')}>
              <Text style={{paddingTop:5, fontSize:17}}>Contact</Text>
              <Icon name='right' size={18} style={{paddingTop: '2%'}} color='#767577'/>
            </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#ffffff'
  },
  IHeader: {
    backgroundColor: '#A5D6AA',
    height: 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  IHeaderText: {
    fontSize: 24,
    paddingLeft: '2%',
    ...Platform.select({
      ios: {            
        marginTop: 25,
      },
      android: {
        marginTop: 20,
      }
    })
  },
  content: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 15
  },
  title: {
    fontSize: 18,
    marginBottom: 10,
   },
  Click: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})