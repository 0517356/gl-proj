import * as React from 'react';
import { StyleSheet, Text, View, Image, Platform, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native'

const Card = (props) => {
	const navigation = useNavigation();
	let transparencyColor;
	Score = props.Score

	if (Score <= 100 && Score >= 70) {
    var transparancyColor = '#52D858'
	} else if(Score < 70 &&  Score >= 50) {
    var transparancyColor = '#F2B05C'
	} else {
    var transparancyColor = '#FF918E'
	} 
	
	return(
		<TouchableOpacity onPress={() => navigation.navigate('Brand', {
			BrandName: props.BrandName, 
			Score:props.Score, 
			Logo: props.Logo,
			Summary: props.Summary,
			Updated: props.Updated,
			Markers: props.Markers
			})}>
			
			<View style={style.cardStyle}>
				<View style={{flexDirection: 'row'}}>
					<Image 
					source={{uri: props.Logo}}
					style={style.imageStyle}
					/>
					<View style={{paddingTop: '1%', paddingLeft: '7%'}}>
						<Text style={style.BrandName}>{props.BrandName}</Text>
						<Text>transparancy:</Text>
					</View>
				</View>
				<View style={style.transparancyScore}>
					<View style={[style.transparancyBox, {shadowColor: transparancyColor}]}>
						<Text style={[style.transparancyText, {color: transparancyColor}]}>{props.Score}%</Text>
					</View>
					<MaterialCommunityIcons style={style.arrowStyle}name="chevron-right" size={25}/>
				</View>
			</View>
		</TouchableOpacity>
	);
}

export default Card

const style = StyleSheet.create ({
	cardStyle: {
		paddingLeft: '3%',
		paddingTop: '5%',	
		justifyContent: 'space-between',
		flexDirection: 'row'
	},
	imageStyle: {
		resizeMode: 'contain',
		...Platform.select({
			ios: {						
				width: 55,
				height: 55,
			},
			android: {
				width: 65,
				height: 65
			}
		})
	},
	BrandName: {
		fontSize: 20,
		paddingBottom: '3%'
	},
	transparancyScore: {
		paddingTop: '6%',
		textAlign: 'center',
		flexDirection: 'row'
	},
	transparancyBox: {
		backgroundColor: '#ffffff',
		height: 20,
		borderRadius: 100,
		width: 50,
		shadowOffset: {
			width: 0,
			height: 12,
		},
		shadowOpacity: 0.58	,
		shadowRadius: 16,
		elevation: 16,
		marginTop: 3
	},
	transparancyText: {
		textAlign: 'center',
		fontSize: 12,
		...Platform.select({
			ios: {						
				paddingTop: '5%',
			},
			android: {
				paddingTop: '3%',
			}
		})
	},
	arrowStyle: {
		paddingLeft: '5%',
		marginRight: '1%',
		alignItems: 'center'
	}
})