import * as React from 'react';
import { SearchBar, ButtonGroup } from 'react-native-elements';
import { StyleSheet, Text, View, Image } from 'react-native';

//view for the header component
export default function HeaderComponent( { selectedIndex, setSelectedIndex, onChangeText, value }) {
   
   return (	
    <View>    
      <View style={style.header}>
      	<Image //logo extracted from '../assets/logo.png'
      		source={require('../assets/logo.png')} 
      		style={style.image}/>
        <SearchBar //searchbar to search with the required props
        	platform='default'
        	containerStyle={style.searchbar}
        	inputContainerStyle={style.searchbarInput}
        	inputStyle={style.inputText}
          onChangeText={onChangeText}
          value={value}
          placeholder="Search here..."
          lightTheme
          round
        />
      </View>
      <View style={style.threeButtons}>
        <ButtonGroup
          containerStyle= {style.buttonContainer}
          buttonStyle={{ width: 60 }}
          buttonContainerStyle={style.buttonContainerStyle}
          textStyle={style.textStyle}
          selectedTextStyle={style.textStyle}
          innerBorderStyle={{color: '#EBEBEB'}}
          buttons={[
            "A-Z",
            "Highest",
            "Sport",
          ]}
          onPress={selectedIdx =>
          setSelectedIndex(selectedIdx)
          }
          selectedButtonStyle={style.selectedButtonStyle}
          selectedIndex={selectedIndex} 
          setSelectedIndex={setSelectedIndex}
        />
      </View>
    </View>
	);
}

//style props for the header
const style = StyleSheet.create ({
	header: {
  	backgroundColor: '#A5D6AA',
  	height: 65,
  	flexDirection: 'row' //to display it 'rowwise'
  },
  image: {
  	width: '39%',
		height: '60%',
		justifyContent: 'center',
		marginTop: 15,
		marginLeft: '1%',
    resizeMode: 'contain'
  },
  searchbar: {
  	backgroundColor: '#A5D6AA',
  	width: '60%',
  	marginLeft: 'auto',
  	marginTop: 5,
  	borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  searchbarInput: {
  	backgroundColor: '#ffffff',
  	borderRadius: 50,
  	height: 40
  },
  inputText: {
  	fontSize: 14,
    color: 'black'
  },
  threeButtons: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '1%',
  },
  buttonContainer: {
    width: 200,
    borderRadius: 15,
    height: 35,
    borderWidth: 5,
    borderColor:'#EBEBEB',
  },
  buttonContainerStyle: {
    width: 60,
    backgroundColor: '#EBEBEB',
    borderWidth: 0,
    alignItems: 'center'
  },
  selectedButtonStyle: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
  },
  textStyle: {
    color: '#000000',
  },
})