## Installation guide

Make sure you have installed node.js. If you have not, install it via https://nodejs.org/en/ (version 14.17.0 is good). Make sure you also cloned the project to your own computer.
<br>
When installed, follow the installation wizard to setup node.js. Install the npm (package manager), if asked.
<br><br>
Open the <i>node.js command prompt</i> and type 
<br><br>
```npm install -g expo-cli```
<br><br>
When done, go to the path on your computer the code from GitLab is on, e.g. C:\Users\{yourname}\documents\gl-proj\TraCloSo by typing:
<br><br>
```cd {your path}```
<br><br>
To install the required modules and dependencies you have to run the following command. You might have to do this later on in the project as well!
<br><br>
```npm install```
<br><br>
Open it by typing
<br><br>
```npm start```
<br><br>
To preview the project you need to download Expo Client App on your smartphone. Depending on the phone you have you can do this on the App Store or the Google Play Store.
Once you have the Expo Client App installed scan the QR Code that you saw when you ran npm start on the terminal. You will have to wait a couple of minutes at first while your project bundles and loads for the first time. After it has completed loading you will see a short message in the center of the screen.<br><br>
In case you want to use a simulator, download Xcode for IOS and Android Studio for Android<br>

<i>If you are using Windows as an operating system you will not be able to download Xcode (due to Apple’s restrictions). Therefore, you need a physical IOS device</i>
<br><br>
Open  the folder TraCloSo in the editor of your choice (e.g. Sublime Text or Visual Code), and open App.js. There you see the text. You can edit it, save it, and the new text will be displayed on your screen! +1
